#include <iostream>
#include <ctime>

#include "Sorts.cpp"

int main()
{

  void (*sortingFunctions[6])(int *, int);
  sortingFunctions[1] = &shellSortSedg;
  sortingFunctions[2] = &shellSort;
  sortingFunctions[3] = &radixSort;
  sortingFunctions[4] = &shellSortHibb2;
  sortingFunctions[5] = &newBucketSort;

  int radixTime = 0;
  int sedgTime = 0;
  int shellTime = 0;
  int countTime = 0;
  int hibbTime = 0;
  int bucketTime = 0;

  const int NUM_SORT = 500; //константа, которая определяет количество проверок времени работы сортировок
  const int N = 40000; //константа определяет размер массива, создающегося для проверки временной сложности алгоритмов сортировок
  const int MAXI = 250000;  //константа заведена для создания массива со значениями от нуля до неё
  for (int i = 0; i < 6 ; i++)
  {
    for (int k = 0; k < NUM_SORT; k++)
    {
      if (i == 0)
      {
        int *array = new int[N];
        for (int j = 0; j < N; j++)
          array[j] = 0 + rand() % MAXI;

//      countingSort(array, n); // для лучшего времени

//      shellSortSedg2(array, n); // для худшего времени

        int start_time = clock(); // начальное время
        countingSort(array, N, MAXI);
        int end_time = clock(); // конечное время
        isSorted(array, N); //выведет "Not sorted", если какой-либо из массивов не будет отсортирован
        delete[]array;
        int search_time = end_time - start_time; // искомое время
        countTime += search_time;
      } else
      {
        int *array = new int[N];
        for (int j = 0; j < N; j++)
          array[j] = 0 + rand() % 250000;

//      countingSort(array, n); // для лучшего времени

//      shellSortSedg2(array, n); // для худшего времени

        int start_time = clock(); // начальное время
        (*sortingFunctions[i])(array, N);
        int end_time = clock(); // конечное время
        isSorted(array, N); //выведет "Not sorted", если какой-либо из массивов не будет отсортирован
        delete[]array;
        int search_time = end_time - start_time; // искомое время
        switch (i)
        {
          case 1: sedgTime += search_time;
            break;
          case 2: shellTime += search_time;
            break;
          case 3: radixTime += search_time;
            break;
          case 4: hibbTime += search_time;
            break;
          case 5: bucketTime += search_time;
            break;
          default:return -1;
        }
      }
    }
  }
  std::cout << "With random elements 0-249999, array length = " << N << " we got results:\n" <<
            "|  radixSort  | " << radixTime / NUM_SORT << "\n" <<
            "|  shellSedg  | " << sedgTime / NUM_SORT << "\n" <<
            "|  shellSort  | " << shellTime / NUM_SORT << "\n" <<
            "|  countSort  | " << countTime / NUM_SORT << "\n" <<
            "|  hibbSort   | " << hibbTime / NUM_SORT << "\n" <<
            "|  bucketSort | " << bucketTime / NUM_SORT << "\n";

  return 0;
}
