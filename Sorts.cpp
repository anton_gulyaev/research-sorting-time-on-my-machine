//
// Created by Антоша on 22.03.2020.
//
using namespace std;
#include <random>
#include <cmath>

int maxi(int *array, int n)
{
  int maximum = array[0];
  for (int i = 1; i < n; i++)
  {
    maximum = (maximum > array[i] ? maximum : array[i]);
  }
  return maximum;
}

void countingSort(int *a, int n, int k)
{
  int *c = new int[k + 1];
  for (int i = 0; i < k; i++)
  {
    c[i] = 0;
  }
  for (int i = 0; i < n; i++)
  {
    c[a[i]] = c[a[i]] + 1;
  }
  int i = 0;
  for (int j = 0; j < k; j++)
  {
    while (c[j] != 0)
    {
      a[i] = j;
      c[j]--;
      i++;
    }
  }
  delete[] c;
}

template<class T>
void print(T *array, int n)
{
  for (int i = 0; i < n; i++)
  {
    std::cout << "array[" << i << "] : " << array[i] << "\n";
  }
  std::cout << "\n";
}

template<class T>
bool isSorted(const T *array, int n)
{
  for (int i = 0; i < n - 1; i++)
  {
    if (array[i] > array[i + 1])
    {
      std::cout << "Not Sorted\n";
      std::cout << "array[" << i << "] : " << array[i] << " > array[" << i + 1 << "] : " << array[i + 1] << "\n";
      return false;
    }
  }
  return true;
}

int binarySearch(const int *array, int key, int l, int r)
{
  int mid = -1;
  while (l <= r)
  {
    mid = l + (r - l) / 2;
    if (array[mid] == key)
    {
      return mid + 1;
    }
    if (array[mid] < key)
    {
      l = mid + 1;
    } else
    {
      r = mid - 1;
    }
  }
}

void binaryInsertionSort(int *array, int n)
{
  for (int i = 1; i < n; i++)
  {
    int j = i - 1;
    int key = array[i];
    int l = 0;
    int mid = -1;
    int r = j;
    mid = binarySearch(array, key, 0, j);
    while (j >= mid)
    {
      array[j + 1] = array[j];
      j--;
    }
    array[j + 1] = key;
  }
}

void shellSort(int *array, int n)
{
  for (int h = n / 2; h > 0; h /= 2)
  {
    for (int i = h; i < n; i++)
    {
      int temp = array[i];
      int j;
      for (j = i; j >= h && array[j - h] > temp; j -= h)
      {
        array[j] = array[j - h];
      }
      array[j] = temp;
    }
  }
}

int incrementSedg(long inc[], int size)
{
  int p1, p2, p3, s;
  p1 = p2 = p3 = 1;
  s = -1;
  do
  {
    if (++s % 2)
    {
      inc[s] = 8 * p1 - 6 * p2 + 1;
    } else
    {
      inc[s] = 9 * p1 - 9 * p3 + 1;
      p2 *= 2;
      p3 *= 2;
    }
    p1 *= 2;
  } while (3 * inc[s] < size);

  return s > 0 ? --s : 0;
}

void shellSortSedg(int *a, int size)
{
  long inc, i, j, seq[40];
  int s;

  // вычисление последовательности приращений
  s = incrementSedg(seq, size);
  while (s >= 0)
  {
    // сортировка вставками с инкрементами inc[]
    inc = seq[s--];

    for (i = inc; i < size; i++)
    {
      int temp = a[i];
      for (j = i - inc; (j >= 0) && (a[j] > temp); j -= inc)
        a[j + inc] = a[j];
      a[j + inc] = temp;
    }
  }
}
void shellSortSedg2(int *a, int size)
{
  long inc, i, j, seq[40];
  int s;

  // вычисление последовательности приращений
  s = incrementSedg(seq, size);
  while (s >= 0)
  {
    // сортировка вставками с инкрементами inc[]
    inc = seq[s--];

    for (i = inc; i < size; i++)
    {
      int temp = a[i];
      for (j = i - inc; (j >= 0) && (a[j] < temp); j -= inc)
        a[j + inc] = a[j];
      a[j + inc] = temp;
    }
  }
}

long incrementHibb(long inc, int size, int s)
{
  int i = 0;
  for (i = 0; pow(2, i) - 1 <= size; i++)
  {
    inc = (long) pow(2, i) - 1;
    s++;
  }
  return s;
}

void shellSortHibb2(int* array, int size) {
  int h_size;

  h_size = floor(log(size+1)/log(2));
  int *h_inc = new int [h_size] ;
  for(int i=0; i < h_size; i++) {
    h_inc[i] = pow(2, i+1) - 1;
  }

  int i, j, tmp;

  for (int r = (h_size - 1); r >= 0; r--) {
    int gap = h_inc[r];

    for(i = gap; i < size; i++) {
      tmp = array[i];

      for(j = i; j >= gap && tmp < array[j-gap]; j-=gap) {
        array[j] = array[j-gap];
      }
      array[j] = tmp;
    }
  }
  free(h_inc);
}

void newBucketSortReq(int *l, int *r, int *temp)
{
  if (r - l <= 64)
  {
    int n = r - l;
    binaryInsertionSort(l, n);
    return;
  }
  int minz = *l, maxz = *l;
  bool is_sorted = true;
  for (int *i = l + 1; i < r; i++)
  {
    minz = min(minz, *i);
    maxz = max(maxz, *i);
    if (*i < *(i - 1)) is_sorted = false;
  }
  if (is_sorted) return;
  int diff = maxz - minz + 1;
  int numbuckets;
  if (r - l <= 1e7) numbuckets = 1500;
  else numbuckets = 3000;
  int range = (diff + numbuckets - 1) / numbuckets;
  int *cnt = new int[numbuckets + 1];
  for (int i = 0; i <= numbuckets; i++)
    cnt[i] = 0;
  int cur = 0;
  for (int *i = l; i < r; i++)
  {
    temp[cur++] = *i;
    int ind = (*i - minz) / range;
    cnt[ind + 1]++;
  }
  int sz = 0;
  for (int i = 1; i <= numbuckets; i++)
    if (cnt[i]) sz++;
  int *run = new int[sz];
  cur = 0;
  for (int i = 1; i <= numbuckets; i++)
    if (cnt[i]) run[cur++] = i - 1;
  for (int i = 1; i <= numbuckets; i++)
    cnt[i] += cnt[i - 1];
  cur = 0;
  for (int *i = l; i < r; i++)
  {
    int ind = (temp[cur] - minz) / range;
    *(l + cnt[ind]) = temp[cur];
    cur++;
    cnt[ind]++;
  }
  for (int i = 0; i < sz; i++)
  {
    int r = run[i];
    if (r != 0) newBucketSortReq(l + cnt[r - 1], l + cnt[r], temp);
    else newBucketSortReq(l, l + cnt[r], temp);
  }
  delete[] run;
  delete[] cnt;
}
void newBucketSort(int *l, int n)
{
  int *r = (l + n);
  int *temp = new int[r - l];
  newBucketSortReq(l, r, temp);
  delete[] temp;
}
//void radixSort(int *array, int n)     //показала себя чуть менее чем на 40% хуже иной radixSort
//{
//  int maxi = INT32_MIN;
//  for (int i = 0; i < n; i++)
//  {
//    if (maxi < array[i])
//    {
//      maxi = array[i];
//    }
//  }
//  for (int exp = 1; maxi / exp > 0; exp *= 10)
//  {
//    countSort(array, exp, n);
//  }
//}
int digit(int n, int k, int N, int M)
{
  return (n >> (N * k) & (M - 1));
}
void radixSortReq(int *l, int *r, int N)
{
  int k = (32 + N - 1) / N;
  int M = 1 << N;
  int sz = r - l;
  int *b = new int[sz];
  int *c = new int[M];
  for (int i = 0; i < k; i++)
  {
    for (int j = 0; j < M; j++)
      c[j] = 0;
    for (int *j = l; j < r; j++)
      c[digit(*j, i, N, M)]++;
    for (int j = 1; j < M; j++)
      c[j] += c[j - 1];
    for (int *j = r - 1; j >= l; j--)
      b[--c[digit(*j, i, N, M)]] = *j;
    int cur = 0;
    for (int *j = l; j < r; j++)
      *j = b[cur++];
  }
  delete[] b;
  delete[] c;
}
void radixSort(int *l, int n)
{
  int *r = (l + n);
  radixSortReq(l, r, 8);
}